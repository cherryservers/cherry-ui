import { version } from './package.json'

const defaultOptions = {
  context: null,
  size: 'normal',
  color: 'blue'
}

const install = (Vue, options) => {
  Vue.prototype.$CHERRY = {
    version,
    ...defaultOptions,
    ...options
  }
}

const CherryUI = {
  install
}

export default CherryUI
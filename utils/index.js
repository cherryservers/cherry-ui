const extractSelectedComponents = (options) => {
  let components = {}
  if (options !== undefined) {
    options.forEach(component => {
      if(Array.isArray(component.variant_list) && component.variant_list.length) {
        component.variant_list.forEach(option => {
          if (option.selected) components[component.id] = option.id
        })
      }
    }) 
  }
  return components
}

const filterComponents = (components = [], keys = []) => {
  const result = []

  for (const component of components) {
    if (keys.includes(component.slug)) {
      result.push(component)
    }
  }

  return result
}

const beautifyQuery = (query) => {
  let q = JSON.stringify(query).replace(/[{}"]/g,'')
  return q.replace(/[:]/g, '.')
}

const uglifyQuery = (query) => {
  const uglyComponents = query.split(',')
  let components = {}
  uglyComponents.forEach(c => {
    c = c.split('.')
    components[c[0]] = c[1]
  })
  return components
}

const slugifyString = (string) => string.toLowerCase().replace(' ', '_')

const monthlifyPrice = (price) => roundNumber(30.4 * 24 * price, 2)

const parseCustomizerQuery = (query, requestable = false) => {
  const { c, b, r } = query
  const isDefined = (variable) => typeof variable !== 'undefined'
  var parsedQuery = {}

  if (isDefined(c)) parsedQuery[requestable ? 'variant_ids' : 'c'] = uglifyQuery(c)
  if (isDefined(b)) parsedQuery[requestable ? 'cycle_id' : 'b'] = parseInt(b)
  if (isDefined(r)) parsedQuery[requestable ? 'location_name' : 'r'] = parseInt(r) === 1 ? 'LT' : parseInt(r) === 2 ? 'NL' : parseInt(r) === 3 ? 'US' : 'SG'

  return parsedQuery
}

const getRegion = (regions, id) => {
  return regions.find(region => region.id === id)
}

const getSelectedCycle = (pricing, id = false) => {
  let selectedCycleObject = {}
  if (id) {
    selectedCycleObject = pricing.find(cycle => cycle.cycle_id === id || cycle.id === id)
  } else {
    selectedCycleObject = pricing.find(cycle => cycle.selected)
  }

  let cycleName = availableCycles.find(cycle => (selectedCycleObject && cycle.id === selectedCycleObject.id) || (selectedCycleObject && cycle.id === selectedCycleObject.cycle_id))

  let result = {
    ...selectedCycleObject,
    ...cycleName
  }

  return result
}

const availableCycles = [
  { id: 38, name: 'Spot Server', shortname: 'hr' },
  { id: 37, name: 'Hourly', shortname: 'hr' },
  { id: 3, name: 'Monthly', shortname: 'mo' },
  { id: 4, name: 'Quarterly', shortname: 'qr'},
  { id: 5, name: 'Semi-Annually', shortname: 'sa' },
  { id: 6, name: 'Annually', shortname: 'yr' }
]

const availableCyclesObject = {
  1: {
    name: 'Daily',
    shortname: 'd'
  },
  2: {
    name: 'Weekly',
    shortname: 'wk'
  },
  3: {
    name: 'Monthly',
    shortname: 'mo'
  },
  4: {
    name: 'Quarterly',
    shortname: 'qr'
  },
  5: {
    name: 'Semiannually',
    shortname: 'sa'
  },
  6: {
    name: 'Annually',
    shortname: 'yr'
  },
  8: {
    name: 'Triennially',
    shortname: '3 yr'
  },
  9: {
    name: 'Quadrennially',
    shortname: '4 yr'
  },
  10: {
    name: 'Quinquennially',
    shortname: '5 yr'
  },
  11: {
    name: '6 Years',
    shortname: '6 yr'
  },
  12: {
    name: '7 Years',
    shortname: '7 yr'
  },
  13: {
    name: '8 Years',
    shortname: '8 yr'
  },
  14: {
    name: '9 Years',
    shortname: '9 yr'
  },
  15: {
    name: '10 Years',
    shortname: '10 yr'
  },
  16: {
    name: 'Biennially',
    shortname: '2 yr'
  },
  17: {
    name: 'Bimonthly',
    shortname: '2 mo'
  },
  18: {
    name: '4 Months',
    shortname: '4 mo'
  },
  19: {
    name: '5 Months',
    shortname: '5 mo'
  },
  20: {
    name: '7 Months',
    shortname: '7 mo'
  },
  21: {
    name: '8 Months',
    shortname: '8 mo'
  },
  22: {
    name: '9 Months',
    shortname: '9 mo'
  },
  23: {
    name: '10 Months',
    shortname: '10 mo'
  },
  24: {
    name: '11 Months',
    shortname: '11 mo'
  },
  25: {
    name: '13 Months',
    shortname: '13 mo'
  },
  26: {
    name: '14 Months',
    shortname: '14 mo'
  },
  27: {
    name: '15 Months',
    shortname: '15 mo'
  },
  28: {
    name: '16 Months',
    shortname: '16 mo'
  },
  29: {
    name: '17 Months',
    shortname: '17 mo'
  },
  30: {
    name: '18 Months',
    shortname: '18 mo'
  },
  31: {
    name: '19 Months',
    shortname: '19 mo'
  },
  32: {
    name: '20 Months',
    shortname: '20 mo'
  },
  33: {
    name: '21 Months',
    shortname: '21 mo'
  },
  34: {
    name: '22 Months',
    shortname: '22 mo'
  },
  35: {
    name: '23 Months',
    shortname: '23 mo'
  },
  36: {
    name: '5 Minutes',
    shortname: '5 min'
  },
  37: {
    name: 'Hourly',
    shortname: 'hr'
  },
  38: {
    name: 'Spot hourly',
    shortname: 'hr'
  }
}

const availableRegions = [
  { id: 1, iso: 'LT', slug: 'eu_nord_1', name: 'EU-Nord-1', location: 'Lithuania', speedtest_url: 'http://speedtest.lt.cherryservers.com' },
  { id: 2, iso: 'NL', slug: 'eu_west_1', name: 'EU-West-1', location: 'Netherlands', speedtest_url: 'http://speedtest.nl.cherryservers.com' },
  { id: 3, iso: 'US', slug: 'us_chicago_1', name: 'US-Chicago-1', location: 'United States', speedtest_url: 'http://speedtest.us.cherryservers.com' },
  { id: 4, iso: 'SG', slug: 'sg_singapore_1', name: 'SG-Singapore-1', location: 'Singapore', speedtest_url: 'http://speedtest.sg.cherryservers.com' }
]

const plansDefaults = [
  { slug: 'cloud_vps_1', location_name: 'LT' },
  { slug: 'cloud_vps_2', location_name: 'LT' }
]

const plansBenchmarks = [
 { slug: 'e3-1240lv5', benchmark: 3503 },
 { slug: 'e3-1240v3', benchmark: 3604 },
 { slug: 'e3-1240v5', benchmark: 4147 },
 { slug: 'e5-1620v4', benchmark: 4097 },
 { slug: 'e5-1650v2', benchmark: 4880 },
 { slug: 'e5-1650v3', benchmark: 5857 },
 { slug: 'e5-1650v4', benchmark: 6292 },
 { slug: 'e5-1660v3', benchmark: 7241 },
 { slug: 'e5-1660v4', benchmark: 7599 },
 { slug: 'e5-2670', benchmark: 5051 },
 { slug: '2x_e5-2620v2', benchmark: 6314 },
 { slug: '2x_e5-2620v4', benchmark: 9811 },
 { slug: '2x_e5-2630lv4', benchmark: 10341 },
 { slug: '2x_e5-2670v3', benchmark: 12892 },
 { slug: '2x_e5-2680v4', benchmark: 17539 },
 { slug: '2x_e5-2630v3', benchmark: 10006 },
 { slug: '2x_e5-2697v2', benchmark: 12056 },
 { slug: '2x_e5-2650v2', benchmark: 8034 },
 { slug: '2x_e5-2650v4', benchmark: 14167 },
 { slug: '4x_e5-4650v2', benchmark: 13416 },
 { slug: '4x_e5-4620v3', benchmark: 16659 },
 { slug: 'amd_epyc_7402p', benchmark: 21090 },
 { slug: '2x_amd_epyc_7402', benchmark: 31723 },
 { slug: 'amd_epyc_7313p', benchmark: 16500 },
 { slug: 'amd_epyc_7443p', benchmark: 20980 },
 { slug: '2x_amd_epyc_7443', benchmark: 36170 },
 { slug: 'Gold 6130', benchmark: 20942 },
 { slug: 'intel_gold_6230r', benchmark: 15390 },
 { slug: '2x_intel_gold_6230r', benchmark: 24600 },
 { slug: 'Gold 6254', benchmark: 28326 },
 { slug: 'cloud_vps_1', benchmark: 704 },
 { slug: 'cloud_vps_2', benchmark: 1378 },
 { slug: 'cloud_vps_4', benchmark: 1297 },
 { slug: 'cloud_vps_6', benchmark: 2276 },
 { slug: 'cloud_vds_2', benchmark: 1434 },
 { slug: 'cloud_vds_4', benchmark: 2847 },
 { slug: 'cloud_vds_6', benchmark: 4491 },
 { slug: 'cloud_vds_8', benchmark: 5537 },
 { slug: 'smart8', benchmark: 1328 },
 { slug: 'ssd_smart8', benchmark: 978 },
 { slug: 'smart16', benchmark: 1328 },
 { slug: 'ssd_smart16', benchmark: 978 },
 { slug: 'quad_smart', benchmark: 1397 },
 { slug: 'hexa_smart', benchmark: 2276 },
 { slug: 'storage_vps_2', benchmark: 704 },
 { slug: 'storage_vps_4', benchmark: 704 },
 { slug: 'storage_vps_6', benchmark: 1378 },
 { slug: 'storage_vps_8', benchmark: 1700 },
]

const roundNumber = (value, decimals = 2, maxDecimals = 7, trailingZeros = true) => {
  let precision = decimals
  let roundedNumber = Number(Math.round(`${value}e${precision}`) + `e-${precision}`)
  
  if (roundedNumber === 0) {
    for (precision; precision <= maxDecimals; precision++) {
      roundedNumber = Number(Math.round(`${value}e${precision}`) + `e-${precision}`)
      if (roundedNumber > 0) break
    }
  }

  roundedNumber = roundedNumber.toFixed(precision)

  if (!trailingZeros) {
    roundedNumber = roundedNumber.replace(precision > 2 ? /[.,]00$|0$/ : /[.,]00$/, '')
  }

  return roundedNumber
}

const getComponentNameByKey = (key) => {
  switch (key) {
    case 'cpu':
    case 'cpu_list':
      return 'CPU'
    case 'gpu':
      return 'GPU'
    case 'core_list':
      return 'Cores'
    case 'memory':
    case 'ram_list':
      return 'RAM'
    case 'nics':
      return 'NICS'
    case 'bandwidth':
      return 'Bandwidth'
    case 'image':
      return 'Image'
    case 'storage':
      return 'Storage'
    case 'NVME':
      return 'NVMe'
    case 'vlan':
      return 'VLan'
    case 'power':
      return 'Power'
    case 'kvm':
      return 'KVM'
    case 'raid_level':
      return 'Raid level'
    case 'raid_type':
      return 'Raid type'
    case 'pcie_storage':
      return 'PCIE storage'
    case 'usb_key':
      return 'USB Key'
    case 'additional_ips':
      return 'Additional IPs'
    case 'r1soft':
      return 'R1Soft'
    case 'r1soft_license':
      return 'R1Soft license'
    case 'control_panel':
      return 'Control panel'
    default:
      return key
  }
}

export {
  getComponentNameByKey,
  getRegion,
  getSelectedCycle,
  availableCycles,
  availableCyclesObject,
  availableRegions,
  plansDefaults,
  plansBenchmarks,
  extractSelectedComponents,
  filterComponents,
  beautifyQuery,
  uglifyQuery,
  slugifyString,
  monthlifyPrice,
  parseCustomizerQuery,
  roundNumber
}

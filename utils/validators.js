const passwordDescriptor = [
  { required: true, message: 'Password is required', trigger: 'blur' },
  { type: 'string', pattern: /(?=.*[a-z])/, message: 'Password must contain lower-case letters (a-z)', trigger: ['blur', 'change'] },
  { type: 'string', pattern: /(?=.*[A-Z])/, message: 'Password must contain upper-case letters (A-Z)', trigger: ['blur', 'change'] },
  { type: 'string', pattern: /(?=.*\d)/, message: 'Password must contain numbers (0-9)', trigger: ['blur', 'change'] },
  { type: 'string', pattern: /.{8,}$/, message: 'Password must be at least 8 characters long', trigger: ['blur', 'change'] }
]

export {
  passwordDescriptor
}

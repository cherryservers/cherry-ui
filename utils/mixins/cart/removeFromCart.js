const removeFromCart = {
  methods: {
    async removeFromCart (item) {
      try {
        this.$wait.start(`CART_ITEM_${item.id}`)
        await this.$store.dispatch('cart/removeItem', item)
        return Promise.resolve(item)
      } catch (e) {
        this.$message.error({
          message: e.response.data.message
        })
        return Promise.reject(e)
      } finally {
        this.$wait.end(`CART_ITEM_${item.id}`)
      }
    }
  }
}

export default removeFromCart

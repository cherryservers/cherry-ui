const addToCart = {
  methods: {
    async addToCart (plan, config = {}, before = () => { Promise.resolve() }) {
      try {
        this.$wait.start(`CART_ADD_${plan.id}`)
        await before()
        await this.$store.dispatch('cart/addItem', { plan, config })
        return Promise.resolve({ plan, config })
      } catch (e) {
        this.$message.error({
          message: e.response.data.message
        })
        return Promise.reject(e)
      } finally {
        this.$wait.end(`CART_ADD_${plan.id}`)
      }
    },
    async onAddToCart ({ plan, config, before }) {
      await this.addToCart(plan, config, before)
    }
  }
}

export default addToCart


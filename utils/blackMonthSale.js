export const discounts = [
  {
    plans: [
      'cloud_vps_1',
      'cloud_vps_2',
      'cloud_vps_4',
      'cloud_vps_6',
    ],
    cycles: {
      3: {
        discount: 0.1
      },
      4: {
        discount: 0.2
      },
      5: {
        discount: 0.3
      },
      6: {
        discount: 0.4
      }
    }
  },
  {
    plans: [
      'cloud_vds_2',
      'cloud_vds_4',
      'smart8',
      'ssd_smart8',
      'smart16',
      'ssd_smart16',
      'quad_smart',
      'hexa_smart'
    ],
    cycles: {
      3: {
        discount: 0.05
      },
      4: {
        discount: 0.1
      },
      5: {
        discount: 0.15
      },
      6: {
        discount: 0.20
      }
    }
  },
  {
    plans: [
      'cloud_vds_6',
      'cloud_vds_8',
    ],
    cycles: {
      3: {
        discount: 0.1
      },
      4: {
        discount: 0.2
      },
      5: {
        discount: 0.25
      },
      6: {
        discount: 0.30
      }
    }
  }
]

export const getPlanDiscount = (slug, cycleId) => {
  const planDiscount = discounts.find(discount => discount.plans.includes(slug))

  if (planDiscount && cycleId in planDiscount.cycles) {
    return planDiscount.cycles[cycleId].discount
  }

  return undefined
}

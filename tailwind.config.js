module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true
  },
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    layers: ['utilities'],
    content: [
      '**/*.vue',
      'node_modules/cherry-ui/**/*.vue'
    ],
    options: {
      whitelistPatterns: [
        /^el-.*/
      ]
    }
  },
  corePlugins: {
    container: false
  },
  theme: {
    extend: {
      inset: {
        '1/2': '50%'
      },
      opacity: {
        '5': '0.05',
        '10': '0.1',
        '20': '0.2'
      },
      zIndex: {
        '3000': 3000,
        '4000': 4000
      },
      animation: {
        'horizontal-scroll': 'horizontal-scroll 120s linear infinite',
        'promo-scroll': 'horizontal-scroll 15s linear infinite',
      },
      keyframes: {
        'horizontal-scroll': {
          'to': { transform: 'translate3d(-100%, 0, 0)' }
        }
      },
      transitionProperty: {
        'border': 'border',
      },
      borderRadius: {
        'none': '0',
        'xs': '2px',
        'sm': '4px',
        'default': '6px',
        'full': '9999px',
        'circle': '9999px'
      },
    },
    alphaColors: ['red.base'],
    alphaValues: [0.02, 0.04, 0.06, 0.08, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9],
    colors: {
      transparent: 'transparent',
      white: '#fff',
      black: '#000',
      gray: '#6f6f70',
      gr: {
        50: '#f9fafb',
        100: '#f3f4f6',
        200: '#e5e7eb',
        300: '#d1d5db',
        400: '#9ca3af',
        500: '#6b7280',
        600: '#4b5563',
        700: '#374151',
        800: '#1f2937',
        900: '#111827',
      },
      red: {
        l5: '#ff6465',
        l4: '#ff5051',
        l3: '#ff3c3d',
        l2: '#ff2829',
        l1: '#ff1415',
        base: '#ff0001',
        d1: '#eb0000',
        d2: '#d70000',
        d3: '#c30000',
        d4: '#af0000',
        d5: '#9b0000'
      },
      green: {
        l5: '#64ffa7',
        l4: '#50ff93',
        l3: '#3cec7f',
        l2: '#28d86b',
        l1: '#14c457',
        base: '#00b043',
        d1: '#009c2f',
        d2: '#00881b',
        d3: '#007407',
        d4: '#006000',
        d5: '#004c00'
      },
      blue: {
        l5: '#64b8ff',
        l4: '#50a4ff',
        l3: '#3c90ff',
        l2: '#287cff',
        l1: '#1468ff',
        base: '#0054ff',
        d1: '#0040eb',
        d2: '#002cd7',
        d3: '#0018c3',
        d4: '#0004af',
        d5: '#00009b'
      },
      orange: {
        l5: '#ffff90',
        l4: '#ffed7c',
        l3: '#ffd968',
        l2: '#ffc554',
        l1: '#ffb140',
        base: '#ff9d2c',
        d1: '#eb8918',
        d2: '#d77504',
        d3: '#c36100',
        d4: '#af4d00',
        d5: '#9b3900'
      },
      coal: {
        base: '#000d24',
        l1: '#737a87',
        l2: '#8c929c',
        l3: '#a6abb3',
        d1: '#263145',
        d2: '#404a5b',
        d3: '#596170'
      },
      snow: {
        base: '#e6eaee',
        l1: '#f2f4f6',
        l2: '#f4f5f7',
        l3: '#f7f8f9',
        d1: '#eaedf0',
        d2: '#edf0f3',
        d3: '#eff1f4'
      }
    },
    fontFamily: {
      'primary': 'Open Sans',
      'secondary': 'Titillium Web'
    },
    spacing: {
      px: '1px',
      '2px': '2px',
      '0': '0',
      '1': '0.25rem',
      '2': '0.5rem',
      '3': '0.75rem',
      '4': '1rem',
      '5': '1.25rem',
      '6': '1.5rem',
      '8': '2rem',
      '9': '2.25rem',
      '10': '2.5rem',
      '12': '3rem',
      '14': '3.5rem',
      '16': '4rem',
      '18': '4.5rem',
      '20': '5rem',
      '24': '6rem',
      '28': '7rem',
      '32': '8rem',
      '36': '9rem',
      '40': '10rem',
      '44': '11rem',
      '48': '12rem',
      '52': '13rem',
      '56': '14rem',
      '64': '16rem',
      '72': '18rem',
      '80': '20rem',
      '88': '22rem',
      '96': '24rem'
    },
    borderColor: theme => ({
      ...theme('colors'),
      default: theme('colors.snow.d3', 'currentColor')
    }),
    boxShadow: theme => ({
      'default': '0 4px 20px 0 rgba(49,110,255,0.08)',
      'thick': '0 2px 1px 0 rgba(64,64,64,0.05)',
      'raised': '0 2px 1px 0 rgba(64,64,64,0.05), 0 4px 20px 0 rgba(49,110,255,0.08)',
      'poped': '0 8px 20px 0 rgba(49,110,255,0.08)',
      'blue': '0 10px 25px 0 rgba(49,110,255,0.30)',
      'blue-lg': '0 8px 15px 0 rgba(49,110,255,0.1), 0 4px 50px 0 rgba(49,110,255,0.20)',
      'outline-blue': `0 0 0 2px ${theme('colors.blue.base')}`,
      'none': 'none'
    }),
    fontSize: {
      '3xs': '.5rem',
      '2xs': '.625rem', // 10px
      'xs': '.75rem', // 12px
      'sm': '.813rem', // 13px
      'base': '.875rem', // 14px
      'md': '1rem', // 16px
      'lg': '1.125rem',// 18px
      'xl': '1.25rem', // 20px
      '2xl': '1.5rem', // 24px
      '3xl': '2rem', // 32px
      '4xl': '2.5rem', // 40px
      '5xl': '3rem', // 48px
      '6xl': '3.5rem' // 56px
    },
    lineHeight: {
      'none': 1,
      'tight': 1.25,
      'normal': 1.5,
      'relaxed': 1.75,
      'loose': 2
    },
    screens: {
      'sm': '568px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px'
    },
    fontWeight: {
      '100': 100,
      '200': 200,
      '300': 300,
      '400': 400,
      '500': 500,
      '600': 600,
      '700': 700,
      '800': 800,
      '900': 900
    },
    order: {
      first: '-9999',
      last: '9999',
      none: '0',
      '1': '1',
      '2': '2',
      '3': '3',
      '4': '4',
      '5': '5',
      '6': '6',
      '7': '7',
      '8': '8',
      '9': '9',
      '10': '10',
      '11': '11',
      '12': '12',
    },
    pulse: {
      colors: [
        'coal.base',
        'red.base', 
        'blue.base'
      ],
      sizes: {
        'sm': '10px',
        'default': '14px',
        'md': '25px'
      }
    }
  },
  variants: {
    animation: ['responsive'],
    animateStop: ['hover', 'group-hover', 'group-child-hover'],
    display: ['responsive', 'group-hover'],
    position: ['responsive'],
    width: ['responsive'],
    margin: ['responsive', 'first', 'last'],
    padding: ['responsive', 'first', 'last'],
    backgroundColor: ['responsive', 'group-hover', 'group-child-hover', 'hover', 'focus', 'active'],
    textColor: ['responsive', 'group-hover', 'hover', 'focus', 'active'],
    borderWidth: ['responsive', 'first', 'last'],
    opacity: ['responsive', 'group-hover', 'hover'],
    order: ['responsive'],
    pulse: ['responsive'],
    scale: ['responsive', 'hover', 'focus', 'active', 'group-hover', 'group-child-hover'],
    flexWrap: ['responsive']
  },
  plugins: [
    require('tailwindcss-bg-alpha')(),

    function({ addComponents, theme }) {
      addComponents({
        '.container': {
          width: '100%',
          maxWidth: theme('screens.xl'),
          margin: '0 auto',
          padding: '0 1rem',
          '@screen sm': {
            padding: '0 2rem'
          },
          '&--full': {
            maxWidth: 'none'
          },
          '&--narrow': {
            maxWidth: theme('screens.lg')
          }
        }
      })
    },

    function({ addComponents, theme }) {
      addComponents({
        '.link': {
          color: theme('colors.blue.base'),
          textDecoration: 'underline',
          '&:hover': {
            color: theme('colors.blue.d1'),
            textDecoration: 'none'
          },
          '&:active': {
            color: theme('colors.blue.d2')
          },
          '&:focus': {
            color: theme('colors.blue.d3')
          }
        }
      })
    },

    function({ addVariant, e }) {
      addVariant('group-child-hover', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.group:hover .${e(`group-child-hover${separator}${className}`)}:hover`
        })
      })
    },

    function({ addUtilities, variants }) {
      addUtilities({
        ['.animate-stop']: {
          animationPlayState: 'paused'
        }
      }, variants('animateStop'))
    },

    function({ addUtilities, e, theme, variants }) {
      const flattenColorPalette = require('tailwindcss/lib/util/flattenColorPalette').default
      const Color = require('color')

      const pulseColors = flattenColorPalette(theme('pulse.colors'))
      const pulseSizes = theme('pulse.sizes')
      let colors = {}
      let sizes = pulseSizes

      if (pulseColors !== undefined) {
        if (Object.keys(pulseColors).length > 0) {
          for (let index = 0; index < Object.keys(pulseColors).length; index++) {
            const color = pulseColors[index];
            const colorValue = theme(`colors.${color}`)
            const colorName = color.split('.').join('-')
            colors[colorName] = colorValue
          }
        }
      }

      addUtilities({
        [`.${e(`pulse`)}:before`]: {
          position: 'absolute',
          top: '0',
          bottom: '0',
          left: '0',
          right: '0',
          content: '""',
          borderRadius: 'inherit',
          zIndex: '-1'
        }
      }, variants('pulse'))

      Object.keys(colors).forEach((colorKey) => {
        Object.keys(sizes).forEach((sizeKey) => {
          let pulseName = `pulse-${colorKey}${sizeKey !== 'default' ? `-${sizeKey}` : ''}`

          addUtilities({
            [`.${e(pulseName)}:before`]: {
              animationName: pulseName,
              animationDuration: '3s',
              animationDelay: '-3s',
              animationIterationCount: 'infinite'
            }
          }, variants('pulse'))

          addUtilities({
            [`@keyframes ${pulseName}`]: {
              '0%': { boxShadow: `0 0 0 0 ${Color(colors[colorKey]).alpha(.3).rgb().string()}` },
              '70%': { boxShadow: `0 0 0 ${sizes[sizeKey]} ${Color(colors[colorKey]).alpha(0).rgb().string()}` },
              '100%': { boxShadow: `0 0 0 0 ${Color(colors[colorKey]).alpha(0).rgb().string()}` }
            }
          })
        })
      })
    }
  ]
}

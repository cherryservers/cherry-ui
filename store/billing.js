export default function (vm, cookies) {
  return {
    namespaced: true,
    state: {
      billing: null
    },
    actions: {
      'load': async ({ commit }) => {
        const billing = cookies.get('cherryBilling') || null
        if (billing) commit('SET_BILLING', billing)
      },
      'save': async ({ state }) => {
        cookies.set('cherryBilling', state.billing, 60 * 60 * 24 * 365, '/', process.env.NODE_ENV === 'production' ? 'cherryservers.com' : null)
      },
      'setBilling': async ({ commit, dispatch }, billing) => {
        commit('SET_BILLING', billing)
        dispatch('save')
      },
      'removeBilling': async ({ commit, dispatch }, billing) => {
        commit('REMOVE_BILLING', billing)
        dispatch('save')
      }
    },
    mutations: {
      'SET_BILLING': (state, payload) => {
        state.billing = payload
      },
      'REMOVE_BILLING': (state, payload) => {
        state.billing = null
      }
    },
    getters: {
      getBilling: (state) => {
        return state.billing
      }
    }
  }
}

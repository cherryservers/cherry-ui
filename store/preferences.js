export default function (vm, cookies) {
  return {
    namespaced: true,
    state: {},
    actions: {
      'load': async ({ commit }) => {
        const preferences = cookies.get('cherryPreferences') || null
        if (preferences) commit('SET_PREFERENCES', preferences)
      },
      'save': async ({ state }) => {
        cookies.set('cherryPreferences', state, 60 * 60 * 24 * 365, '/', process.env.NODE_ENV === 'production' ? 'cherryservers.com' : null)
      },
      'setPreference': async ({ commit, dispatch }, preference) => {
        commit('SET_PREFERENCE', preference)
        dispatch('save')
      },
      'removePreference': async ({ commit, dispatch }, preferenceKey) => {
        commit('REMOVE_PREFERENCE', preferenceKey)
        dispatch('save')
      }
    },
    mutations: {
      'SET_PREFERENCES': (state, payload) => {
        Object.assign(state, payload)
      },
      'SET_PREFERENCE': (state, payload) => {
        vm.set(state, payload.key, payload.value)
      },
      'REMOVE_PREFERENCE': (state, payload) => {
        vm.delete(state, payload)
      }
    },
    getters: {
      getPreference: (state) => (key) => {
        return state[key]
      }
    }
  }
}

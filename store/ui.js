export default function (vm) {
  return {
    namespaced: true,
    state: {},
    actions: {
      'setKeyValue': async ({ commit }, payload) => {
        commit('SET_KEY_VALUE', payload)
      },
      'removeKey': async ({ commit }, payload) => {
        commit('REMOVE_KEY', payload)
      }
    },
    mutations: {
      'SET_KEY_VALUE': (state, payload) => {
        vm.set(state, payload.key, payload.value)
      },
      'REMOVE_KEY': (state, payload) => {
        vm.delete(state, payload)
      }
    },
    getters: {
      getKeyValue: (state) => (key) => {
        return state[key]
      }
    }
  }
}

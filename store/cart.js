import petname from 'node-petname'

const getURL = (sessionHash = null, teamID = null) => {
  if (sessionHash && sessionHash !== 'anonymous' && teamID && teamID !== 'anonymous') return `/clients/${sessionHash}/teams/${teamID}`
  return ''
}

export default function (vm, apiInstance) {
  return {
    namespaced: true,
    state: {
    },
    actions: {
      'setAPIInstance': async (payload) => {
        apiInstance = payload.api
      },
      'create': async ({ commit, getters, rootGetters }) => {
        try {
          const discount = await rootGetters['preferences/getPreference']('discount')
          const { data } = await apiInstance.post(`v1/carts`, { discount })
          commit('UPDATE_CART', {cart: data, teamID: getters.teamID})
          return Promise.resolve(data)
        } catch (e) {
          console.log(e)
          return Promise.reject(e)
        }
      },
      'loadCart': async ({ commit, dispatch, getters }, cartHash) => {
        try {
          if (cartHash !== undefined) {
            const { data } = await apiInstance.get(`v1${getURL(getters.sessionHash, getters.teamID)}/carts/${cartHash}?currency=${getters.currency}`)
            commit('UPDATE_CART', {cart: data, teamID: getters.teamID})
            return Promise.resolve(data)
          } else {
            return await dispatch('create')
          }
        } catch (e) {
          dispatch('destroyCart')
          if (e.response.data.code === 404) return await dispatch('create')
        }
      },
      'destroyCart': async ({ commit, getters }, teamDelete = false) => {
        try {
          let param = getters.teamID
          if (teamDelete) param = teamDelete
          commit('DESTROY_CART', param)
          return Promise.resolve()
        } catch (e) {
          console.log(e)
          return Promise.reject(e)
        }
      },
      'getCart': async ({ commit, dispatch, getters }, team = false) => {
        try {
          let teamParam = team
          if(!teamParam) teamParam = getters.teamID
          if (getters.carts[teamParam] !== undefined && getters.carts[teamParam].id !== undefined) {
            const { data } = await apiInstance.get(`v1${getURL(getters.sessionHash, teamParam)}/carts/${getters.carts[teamParam].id}?currency=${getters.currency}`)
            commit('UPDATE_CART', {cart: data, teamID: getters.teamID})
            return Promise.resolve(data)
          } else {
            return await dispatch('create')
          }
        } catch (e) {
          await dispatch('destroyCart')
          console.log(e)
          return Promise.reject(e)
        }
      },
      'addItem': async ({ commit, dispatch, getters }, payload) => {
        // if cart for this team exist - add item
        try {
          if (getters.carts[getters.teamID] === undefined) {
            await dispatch('create')
          }
          const { data } = await apiInstance.post(`/v1${getURL(getters.sessionHash, getters.teamID)}/carts/${getters.carts[getters.teamID].id}/items`, {
            plan_id: payload.plan.id,
            cycle_id: payload.config.cycle,
            region: payload.config.region ? payload.config.region.name : payload.plan.region.name,
            variant_id_list: payload.config.components ? Object.values(payload.config.components) : [],
            variant_comment_list: payload.config ? payload.config.comments : [],
            ip_addresses: payload.config.ip_addresses,
            ssh_keys: payload.config.ssh_keys,
            hostname: payload.config.hostname || petname(2, '-'),
            project_id: getters.projectID,
            currency: getters.currency,
            user_data: payload.config.user_data,
            os_partition_size: payload.config.os_partition_size
          })
          commit('UPDATE_CART', {cart: data, teamID: getters.teamID})
          return Promise.resolve(data)
        } catch (e) {
          console.log(e)
          return Promise.reject(e)
        }
      },
      'editItem': async ({ commit, getters }, payload) => {
        try {
          const { data } = await apiInstance.patch(`/v1${getURL(getters.sessionHash, getters.teamID)}/carts/${getters.carts[getters.teamID].id}/items/${payload.id}`, {
            plan_id: payload.plan_id,
            cycle_id: payload.price.cycle_id,
            region: payload.region.name,
            variant_id_list: payload.selected_variants,
            variant_comment_list: payload.comments,
            ssh_keys: payload.ssh_keys,
            ip_addresses: payload.ip_addresses,
            hostname: payload.hostname,
            currency: getters.currency,
            user_data: payload.user_data
          })
          commit('UPDATE_CART', {cart: data, teamID: getters.teamID})
          return Promise.resolve(data)
        } catch (e) {
          console.log(e)
          return Promise.reject(e)
        }
      },
      'removeItem': async ({ commit, getters }, payload) => {
        try {
          const { data } = await apiInstance.delete(`/v1${getURL(getters.sessionHash, getters.teamID)}/carts/${getters.carts[getters.teamID].id}/items/${payload.id}`, {
            data: {
              currency: getters.currency
            }
          })
          commit('UPDATE_CART', {cart: data, teamID: getters.teamID})
          return Promise.resolve(data)
        } catch (e) {
          console.log(e)
          return Promise.reject(e)
        }
      },
      'applyDiscount': async ({ commit, getters }, discountCode) => {
        try {
          const { data } = await apiInstance.patch(`/v1${getURL(getters.sessionHash, getters.teamID)}/carts/${getters.carts[getters.teamID].id}`, {
            discount: discountCode,
            currency: getters.currency
          })
          commit('UPDATE_CART', {cart: data, teamID: getters.teamID})
          return Promise.resolve(data)
        } catch (e) {
          console.log(e)
          return Promise.reject(e)
        }
      },
      'checkCredit': async ({ getters }, hourlyAmount) => {
        try {
          const {data} = await apiInstance.get(`/v1/clients/${getters.sessionHash}/enough-credit`, {
            params: {
              hourly_amount: hourlyAmount,
              team_id: getters.teamID,
              currency: getters.currency
            }
          })
          return Promise.resolve({ creditEnough: data.enough_credit, requiredTopupAmount: data.required_amount_topup })
        }
        catch (e) {
          console.log(e)
          return Promise.reject(e)
        }
      },
      'syncCart': async ({ commit, getters }) => {
        try {
          if (getters.sessionHash !== 'anonymous' && getters.carts[getters.teamID] !== undefined && getters.carts[getters.teamID].id !== undefined && getters.teamID !== 'anonymous') {
            let reqParams = {
              currency: getters.currency
            }
            getters.projectID === 'anonymous' ? reqParams.project_id = 0 : reqParams.project_id = getters.projectID
            const {data} = await apiInstance.post(`/v1${getURL(getters.sessionHash, getters.teamID)}/carts/${getters.carts[getters.teamID].id}/sync`, reqParams)
            commit('UPDATE_CART', {cart: data, teamID: getters.teamID})
            return Promise.resolve(data)
          }
          return Promise.resolve()
        } catch (e) {
          console.log(e)
          return Promise.reject(e)
        }
      },
      'transferCart': async ({ commit, getters }, payload) => {
        if (payload.oldTeamID) {
          payload.teamID = getters.teamID
          payload.cartObject ? commit('TRANSFER_CART', payload) : commit('TRANSFER_CART', {cartObject: getters.activeCart, oldTeamID: payload.teamID})
        }
        return Promise.resolve()
      },
      'open': ({ commit, getters }) => {
        if (getters.activeCart) commit('MINI_CART_TOGGLE', {teamID: getters.teamID, status: true})
        return Promise.resolve()
      },
      'close': ({ commit, getters }) => {
        if (getters.activeCart) commit('MINI_CART_TOGGLE', {teamID: getters.teamID, status: false})
        return Promise.resolve()
      },
      'toggle': ({ dispatch, getters }) => {
        if (getters.activeCart && getters.activeCart.minicart) {
          return dispatch('close')
        }
        return dispatch('open')
      },
      'fillFromStorage': async ({ commit }, payload) => {
        payload.minicart = false
        commit('UPDATE_CART', {cart: payload, teamID: 'anonymous'})
        return Promise.resolve()
      }
    },
    mutations: {
      'DESTROY_CART': (state, teamID) => {
        if (state[teamID]) vm.delete(state, teamID)
      },
      'UPDATE_CART': (state, payload) => {
        if (payload.cart.minicart === undefined) payload.cart.minicart = true
        vm.set(state, payload.teamID, payload.cart)
      },
      'TRANSFER_CART': (state, payload) => {
        vm.set(state, payload.teamID, payload.cartObject)
        if (payload.teamID !== payload.oldTeamID) vm.delete(state, payload.oldTeamID)
      },
      'MINI_CART_TOGGLE': (state, payload) => {
        state[payload.teamID].minicart = payload.status
      }
    },
    getters: {
      carts: state => state,
      activeCart: (state, getters) => {
        return state[getters.teamID] ? state[getters.teamID] : false
      },
      isCartAnonymous: (state, getters) => {
        return getters.carts['anonymous'] ? true : false
      },
      cartExist: (state, getters) => {
        return !!getters.carts[getters.teamID]
      },
      shouldPayInvoice: (state, getters) => {
        // if has long tearm servers or custom plans in cart
        if (getters.activeCart && (getters.activeCart.cart_long_term_without_vat > 0 || getters.activeCart.cart_fee_fixed > 0)) {
          return true
        }
        return false
      },
      hasCustomPlans: (state, getters) => {
        if (getters.activeCart && getters.activeCart.item_list.length) {
          return !!getters.activeCart.item_list.filter(plan => plan.is_custom === 1).length
        }
        return false
      },
      sessionHash: (state, getters, rootState, rootGetters) => {
        return rootGetters['isAuth'] ? rootGetters['isAuth'].hash : 'anonymous'
      },
      currency: (state, getters, rootState, rootGetters) => {
        return rootGetters['currency/active'] ? rootGetters['currency/active'] : 'EUR'
      },
      teamID: (state, getters, rootState, rootGetters) => {
        return rootGetters['portal/team'] ? rootGetters['portal/team'].id : 'anonymous'
      },
      projectID: (state, getters, rootState, rootGetters) => {
        return rootGetters['portal/project'] ? rootGetters['portal/project'].id : 'anonymous'
      }
    }
  }
}

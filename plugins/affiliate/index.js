import AffiliatePlugin from './plugin'

const defaultConfig = {
  router: null,
  cookies: null,
  store: null,
  privateAPI: null
}

export default function install (Vue, initConfig = {}) {
  Vue.prototype.$affiliate = Vue.$affiliate = new AffiliatePlugin({ 
    ...defaultConfig, 
    ...initConfig
  })

  Vue.$affiliate.initVueRouterGuard()
}
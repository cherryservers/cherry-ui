export default class AffiliatePlugin {
  constructor (config) {
    this.router = config.router
    this.cookies = config.cookies
    this.store = config.store
    this.privateAPI = config.privateAPI
  }

  setCookie (affiliateCode) {
    return this.cookies.set('cherryAffiliate', affiliateCode, 60 * 60 * 24 * 90, '/', process.env.NODE_ENV === 'production' ? 'cherryservers.com' : null)
  }

  getCookie () {
    return this.cookies.get('cherryAffiliate') || null
  }

  // trackVisit (sessionHash = null) {
  //   const affiliateCode = this.getCookie()

  //   if (affiliateCode) {
  //     if (!sessionHash) this.privateAPI.post(`v1/affiliates/${affiliateCode}/visits`)
  //     else this.privateAPI.post(`v1/clients/${sessionHash}/affiliates/${affiliateCode}/visits`)
  //   }
  // }

  // trackPlanVisit (planId, sessionHash = null) {
  //   const affiliateCode = this.getCookie()

  //   if (affiliateCode) {
  //     if (!sessionHash) this.privateAPI.post(`v1/affiliates/${affiliateCode}/visits`, { plan_id: planId })
  //     else this.privateAPI.post(`v1/clients/${sessionHash}/affiliates/${affiliateCode}/visits`, { plan_id: planId })
  //   }
  // }

  initVueRouterGuard () {
    this.router.beforeEach((to, from, next) => {
      let query = to.query
      const { affiliate: affiliateCode } = query

      if (affiliateCode) {
        this.setCookie(affiliateCode)
        // this.trackVisit()

        delete query.affiliate

        next({ ...to, ...{ query } })
      }

      next()
    })
  }
}

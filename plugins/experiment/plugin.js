import availableExperiments from './experiments'

export default class ExperimentsPlugin {
  constructor (config) {
    this.appContext = config.appContext
    this.router = config.router
    this.cookies = config.cookies
    this.experiments = {}
    this.experimentsEligible = []
    this.experimentsIndexes = []
    this.classes = []
  }

  getCookie () {
    return this.cookies.get('cherryExperiment') || ''
  }

  setCookie (cookie) {
    return this.cookies.set('cherryExperiment', cookie, 60 * 60 * 24 * 365, '/', process.env.NODE_ENV === 'production' ? 'cherryservers.com' : null)
  }

  removeCookie () {
    return this.cookies.remove('cherryExperiment')
  }

  fromQuery (route) {
    const { exp: queryExperiments } = route.query
    return queryExperiments !== undefined ? this.filterAvailableExperiments(this.parseString(queryExperiments)) : []
  }

  fromCookie (cookie) {
    const cookieExperiments = cookie
    return cookieExperiments !== undefined ? this.filterAvailableExperiments(this.parseString(cookieExperiments)) : []
  }

  parseString (string) {
    if (string) {
      return string.split('&').map((experiment) => {
        const [ id, variants ] = experiment.split('.')
        return { id, variants: variants.split('-').map(v => parseInt(v)) }
      })
    }

    return []
  }

  serializeString (experiments) {
    if (experiments.length) {
      return experiments.map(experiment => experiment.id + '.' + experiment.variants.join('-')).join('&')
    }

    return null
  }

  filterAvailableExperiments (experiments) {
    return experiments.filter(experiment => availableExperiments.some(exp => exp.id === experiment.id))
  }

  filterEligibleExperiments (conditions, experiments) {
    return experiments.filter(exp => {
      const experiment = availableExperiments.find(obj => obj.id === exp.id)
      return !('isEligible' in experiment) || (typeof experiment.isEligible === 'function' && experiment.isEligible({ ...conditions, ...{ appContext: this.appContext } }))
    })
  }

  assignExperiments (conditions) {
    const experimentsFromQuery = this.fromQuery(conditions.route)
    const experimentsFormCookie = this.fromCookie(conditions.cookie)
    
    for (let experimentIndex = 0; experimentIndex < availableExperiments.length; experimentIndex++) {
      let experiment = { ...availableExperiments[experimentIndex] }
      let experimentFromQuery = experimentsFromQuery.find(exp => exp.id === experiment.id)
      let experimentFromCookie = experimentsFormCookie.find(exp => exp.id === experiment.id)
      let persistedVariantIndexes = []

      if (experimentFromCookie) {
        persistedVariantIndexes = experimentFromCookie.variants
      }

      if (experimentFromQuery) {
        persistedVariantIndexes = experimentFromQuery.variants
      }

      if (!experimentFromCookie && !experimentFromQuery && typeof experiment.isEligible === 'function' && !experiment.isEligible({ ...conditions, ...{ appContext: this.appContext } })) {
        continue
      }
      
      let variantIndexes = this.assignVariants(experiment, persistedVariantIndexes)
      let classes = variantIndexes.map(index => 'exp-' + experiment.name + '-' + index)
      
      this.experiments = {
        ...this.experiments, 
        [experiment.id]: {
          id: experiment.id,
          type: experiment.type,
          name: experiment.name,
          variants: experiment.variants,
          variantIndexes,
          classes
        }
      }
    }

    this.experimentsEligible = this.filterEligibleExperiments(conditions, Object.values(this.experiments))
    this.experimentsIndexes = [...Object.keys(this.experiments).map(index => parseInt(index))]
    this.classes = Object.values(this.experiments).reduce((accumulator, exp) => [...accumulator, ...exp.classes], [])

    // const currentExperimentsCookie = this.getCookie()
    // const newExperimentsCookie = Object.keys(this.experiments).length ? this.serializeString(Object.values(this.experiments).map((experiment) => ({ id: experiment.id, variants: experiment.variantIndexes }))) : null

    // if (newExperimentsCookie && newExperimentsCookie !== currentExperimentsCookie) {
    //   this.setCookie(newExperimentsCookie)
    // } else if (currentExperimentsCookie) {
    //   this.removeCookie()
    // }

    return this.experiments
  }

  assignVariants (experiment, persistedVariantIndexes = []) {
    let variantIndexes = []

    if (experiment.type === 'ab') {
      variantIndexes = persistedVariantIndexes.filter(index => experiment.variants[index])

      if (variantIndexes.length < 1) {
        variantIndexes.push(this.weightedRandom(experiment.variants.map(variant => variant.weight === undefined ? 1 : variant.weight)))
      }
    }

  //   if (experiment.type === 'mvt') {
  //   const experimentVariantIndexes = []

  //   for (let secIndex = 0; secIndex < experiment.sections.length; secIndex++) {
  //     let sectionVariantIndexes = [variantIndexes[secIndex]]
  //     sectionVariantIndexes = sectionVariantIndexes.filter((index) => experiment.sections[secIndex].variants[index])

  //     if (sectionVariantIndexes < 1) {
  //       const sectionVariantWeights = experiment.sections[secIndex].variants.map(variant => variant.weight === undefined ? 1 : variant.weight)
  //       sectionVariantIndexes.push(this.weightedRandom(sectionVariantWeights))
  //     }

  //     experimentVariantIndexes.push(sectionVariantIndexes[0])
  //   }

  //   variantIndexes = experimentVariantIndexes
  // }

    return variantIndexes
  }

  handler (route) {
    // There is a chance that the code will be run before the Hotjar script has loaded, to avoid errors this keep this line below
    // eslint-disable-next-line
    window.hj=window.hj||function(){(hj.q=hj.q||[]).push(arguments)}

    Object.values(this.experiments).forEach(experiment => {
      // if (window.dataLayer) {
      //   window.dataLayer.push({
      //     exp: `${experiment.id}.${experiment.variantIndexes.join('-')}`
      //   })
      // }
      if (typeof experiment.hotjarTrigger === 'function' && experiment.hotjarTrigger(route)) {
        window.hj('trigger', `${this.appContext}_${route.name}_${experiment.id}_${experiment.variantIndexes.join('-')}`)
      }
    })
  }

  weightedRandom (weights) {
    let totalWeight = 0
    let i
    let random

    for (i = 0; i < weights.length; i++) {
      totalWeight += weights[i]
    }

    random = Math.random() * totalWeight

    for (i = 0; i < weights.length; i++) {
      if (random < weights[i]) {
        return i
      }

      random -= weights[i]
    }

    return -1
  }

  initVueRouterGuard () {
    this.router.afterEach((to) => {
      if ((this.appContext === 'web' && process.client) || this.appContext === 'portal') {
        Object.keys(this.experiments).length ? this.handler(to) : this.removeCookie()
      }
    })
  }
}

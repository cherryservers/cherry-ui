import ExperimentPlugin from './plugin'

const defaultConfig = {
  appContext: null,
  router: null,
  cookies: null
}

export default function install (Vue, initConfig = {}) {
  Vue.prototype.$exp = Vue.$exp = new ExperimentPlugin({ 
    ...defaultConfig, 
    ...initConfig
  })

  Vue.$exp.initVueRouterGuard()
}
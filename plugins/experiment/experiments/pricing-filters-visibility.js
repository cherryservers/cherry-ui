export default {
  type: 'ab',
  name: 'pricing-filters-visibility',
  id: 'XcjkvL1xSB-fhMvQblV-qA',
  isEligible: ({ appContext, route }) => (appContext === 'web' && route.name === 'pricing-category___en') || (appContext === 'portal' && ['ViewBareMetal', 'ViewVirtual'].includes(route.name)),
  variants: [
    {
      name: 'Original',
      weight: 1
    },
    {
      name: 'Variant 1',
      weight: 1
    }
  ]
}

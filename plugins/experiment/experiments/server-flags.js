export default {
  type: 'ab',
  name: 'server-flags',
  experimentID: 'eWtxyBUaQeS_lwKSb6j5ew',
  variants: [
    {
      name: 'Original',
      weight: 1
    },
    {
      name: 'Variant 1',
      weight: 1
    }
  ]
}

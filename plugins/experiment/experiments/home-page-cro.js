export default {
  type: 'ab',
  name: 'home-cro',
  id: 'VzjziKv-TUKLeA4jaSOEjA',
  // hotjarTrigger: (route) => route.name === 'pricing-category',
  isEligible: ({ appContext, route }) => appContext === 'web' && route.name === 'index',
  variants: [
    {
      name: 'Original',
      weight: 1
    },
    {
      name: 'Variant 1',
      weight: 1
    }
  ]
}

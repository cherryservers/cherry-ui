export default {
  type: 'ab',
  name: 'pricing-cro',
  experimentID: 'Jaxm4QjpRaukr2oD-pxhSw',
  hotjarTrigger: (route) => route.name === 'pricing-category',
  variants: [
    {
      name: 'Original',
      weight: 1
    },
    {
      name: 'Variant 1',
      weight: 1
    }
  ]
}

export default {
  type: 'ab',
  name: 'server-order-form',
  id: 'lVOBCI24QqyljYBiWTtv-g',
  variants: [
    {
      name: 'Original',
      weight: 0.75
    },
    {
      name: 'Variant 1',
      weight: 0.25
    }
  ]
}

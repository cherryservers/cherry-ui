export default {
  type: 'ab',
  name: 'pricing-categories',
  id: 'rBOBCI24KqyljLBiVTtx-g',
  isEligible: ({ appContext, route }) => (appContext === 'web' && route.name === 'bare-metal-dedicated-servers___en' || (route.name === 'pricing-category___en' && route.params.category === 'dedicated-servers')) || (appContext === 'portal' && ['ViewDeployment'].includes(route.name)),
  variants: [
    {
      name: 'Original',
      weight: 1
    },
    {
      name: 'New',
      weight: 1
    }
  ]
}

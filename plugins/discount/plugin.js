export default class DiscountPlugin {
  constructor (config) {
    this.store = config.store
  }
  
  async setDiscount (discountCode) {
    const currentDiscount = this.getDiscount()
    const activeCart = this.store.getters['cart/activeCart']

    if (!currentDiscount || currentDiscount !== discountCode) {
      await this.store.dispatch('preferences/setPreference', { key: 'discount', value: discountCode })
    }

    if (activeCart && !activeCart.cart_discount) {
      await this.store.dispatch('cart/applyDiscount', discountCode)
    }
  }

  getDiscount () {
    return this.store.getters['preferences/getPreference']('discount')
  }

  initStoreSubscriber () {
    this.store.subscribeAction({
      after: (action) => {
        if (action.type === 'cart/applyDiscount') {
          const currentDiscount = this.store.getters['preferences/getPreference']('discount')
          const { cart_discount: cartDiscount } = this.store.getters['cart/activeCart']

          if (cartDiscount && cartDiscount !== currentDiscount) {
            this.store.dispatch('preferences/setPreference', { key: 'discount', value: cartDiscount })
          }
        }
      }
    })
  }
}

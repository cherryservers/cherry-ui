import DiscountPlugin from './plugin'

const defaultConfig = {
  store: null
}

export default function install (Vue, initConfig = {}) {
  Vue.prototype.$discount = Vue.$discount = new DiscountPlugin({
    ...defaultConfig,
    ...initConfig
  })
  // Vue.$discount.initStoreSubscriber()
  // Vue.$discount.setDiscount('AUTUMNSALE2023')
}
import Vue from 'vue'
import CherryUI from '../../index.js'
import '../../styles/main.css'

Vue.use(CherryUI, {
  size: 'large'
})

import Vue from 'vue'
import App from './App'

import './plugins/axios'
import './plugins/portal'
import './plugins/element-ui'
import './plugins/cherry-ui'
import router from './plugins/router'

new Vue({
  router,
  render: h => h(App),
}).$mount('.playground')
